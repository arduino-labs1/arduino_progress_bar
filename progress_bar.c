#ifndef F_CPU
#define F_CPU 16000000UL
#endif

#include <avr/io.h>
#include <util/delay.h>

int COUNT = 5;

void on_light(int number) {
    int port = 1 << 2;
    PORTD = port << number;
    _delay_ms (150);
}

void main() {
    DDRD  = 0b11111100;
    PORTD = 0b00000010;

    int dir = 1;
    int num = 0;
    while (1) {
	if (!(PIND & 0b00000010)) {
	    _delay_ms(30);
	    if (!(PIND & 0b00000010)) {
		dir *= -1;
	    }
	}
	on_light(num);
	num = (num + 1*dir + COUNT) % COUNT;
    }
}

// $ avr-gcc -g -O1 -mmcu=atmega328p -o progress_bar.elf progress_bar.c
// $ avrdude -v -patmega328p -c arduino -P /dev/ttyUSB0 -U flash:w:'progress_bar.elf'
