## Команды:
```bash
$ avr-gcc -g -O1 -mmcu=atmega328p -o progress_bar.elf progress_bar.c
$ avrdude -v -patmega328p -c arduino -P /dev/ttyUSB0 -U flash:w:'progress_bar.elf'  
```
